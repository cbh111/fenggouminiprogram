import request from "../../util/request"

// pages/menu/menu.js
Page({
  data: {
    categoryList: [],
    // 左侧的id
    leftId: 'left1',
    // 左侧的当前项
    leftActiveNum: 1,
    // 右侧的id
    rightId: 'right1',
    // 右侧高度的数组
    heightArr: []
  },
  onLoad() {
    this.getcategoryList()
  },
  // 获取分类的数据
  getcategoryList() {
    request({
      url: '/categories?_embed=goods'
    }).then(res => {
      this.setData({
        categoryList: res
      })
    })
  },
  // 获取每个区块的高度（操作dom），onready生命周期函数类似于mounted
  // vue中的mounted也没有说100%保证所有的组件渲染完，所以onReady也是这样，所以为了确保获取的准确的数据，要在外边加一个定时器，大概1500毫秒左右就可以
  onReady() {
    let _this = this
    setTimeout(() => {
      let initArr = [0] //初始的数组
      let initNum = 0 //初始的数值
      const query = wx.createSelectorQuery()
      query.selectAll('.rightblock').boundingClientRect()
      query.selectViewport().scrollOffset()
      query.exec(function(res){
        //拿到每一个区块的高度，然后存起来
        res[0].map(val => {
          initNum+=val.height //实现高度的累加
          initArr.push(initNum) //将累加出来的高度push到数组中
        })
        _this.setData({
          heightArr: initArr
        })
      })
    },1500)
  },
  // 左侧的点击事件
  leftClickFn(e) {
    this.setData({
      leftActiveNum: e.target.dataset.myid,
      leftId: 'left' + e.target.dataset.myid,
      rightId: 'right' + e.target.dataset.myid
    })
  },
  // 右侧的滚动事件
  rightScrollFn(e) {
    let st = e.detail.scrollTop;
    let myArr = this.data.heightArr;
    for (let i=0; i<myArr.length; i++) {
      if (st>=myArr[i] && st<=myArr[i+1]-1) {
        this.setData({
          leftId: 'left' + i + 1,
          leftActiveNum: i + 1
        })
        return
      }
    }
  },
  // 跳转到商品详情页面
  categoryClick(e) {
    let id = e.currentTarget.dataset.id
    let name = e.currentTarget.dataset.name
    wx.navigateTo({
      url: `/pages/goodsdetail/goodsdetail?id=${id}&name=${name}`
    })
  }
})