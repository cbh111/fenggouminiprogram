// pages/goodsdetail/goodsdetail.js
import checkAuth from '../../util/auth'
import request from '../../util/request'
Page({
  data: {
    // 商品详情的数据
    detailList: null,
    tagList: ['商品详情','用户评价'],
    tagcurrent: 0,
    // 用户评价的数据
    commentList: []
  },
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: options.name
    })
    this.getDetailInfo(options.id)
    this.getCommentInfo()
  },
  // 获取商品详细信息请求
  getDetailInfo(id) {
    request({
      url: `/goods/${id}`
    }).then(res => {
      this.setData({
        detailList: res
      })
    })
  },
  // 获取用户评价的信息
  getCommentInfo() {
    request({
      url: '/comments'
    }).then(res => {
      this.setData({
        commentList: res
      })
    })
  },
  // 点击图片后的处理事件函数
  imgClick(e) {
    wx.previewImage({
      current: e.currentTarget.dataset.current,
      urls: this.data.detailList.slides.map(item => `http://localhost:5000${item}`)
    })
  },
  // tag标签的点击事件
  tagClick(e) {
    this.setData({
      tagcurrent: e.currentTarget.dataset.index
    })
  },
  // 加入购物车
  addCart() {
    // 先用封装的checkAuth进行用户判断，判断是否用户已经登录和手机绑定。
    // 加入购物车里的逻辑应该是后端要进行判断的，比如当前用户购物车里已经有荣耀9x了，那么用户在添加到购物车中，应该是数量加一，而不是在创建一个荣耀9x，但我们用的是json-server服务的后端，所以加入购物的这种逻辑判断由前端来完成。
    checkAuth(() => {
      let {nickName} = wx.getStorageSync('token')
      let tel = wx.getStorageSync('tel')
      let id = this.data.detailList.id
      request({
        url: '/carts',
        data: {
          nickName: nickName,
          tel: tel,
          goodId: id
        }
      }).then(res => {
        if (res.length===0) {
          // 这里代表此账号中购物车里没有这个商品的数据，此时在服务器里添加，购物车里创建一个这个商品
          request({
            url: '/carts',
            method: 'POST',
            data: {
              username: nickName,
              tel: tel,
              goodId: id,
              number: 1,
              checked: false
            }
          }).then (res => {
            wx.showToast({
              title: '添加购物车成功',
            })
          })
        } else {
          // 这里代表此账号购物车里已经有该商品的信息，此时该商品的数量要加一。
          // 因为小程序里不支持catch请求，只能用put,而put请求会覆盖之前的数据，所以在请求中，要把之前的数据先写上，在写要改的数据，则要改的数据会覆盖之前的数据。
          request({
            url: `/carts/${res[0].id}`,
            method: 'PUT',
            data: {
              // 先写上之前的数据
              ...res[0],
              // 在写上要改的数据，在这个data对象中要改的数据number会覆盖上边写展开数据中的number，最后向服务器中提交时，会提交这个新的number,注意number和...res[0]的书写位置不能颠倒
              number: res[0].number + 1
            }
          }).then (res => {
            wx.showToast({
              title: '添加购物车成功',
            })
          })
        }
      })
    })
  },
  // 跳转购物车
  goToCart() {
    wx.switchTab({
      url: '/pages/shopcar/shopcar'
    })
  }
})