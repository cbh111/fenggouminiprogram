// pages/home/home.js
import request from '../../util/request.js'
Page({
  data: {
    bannerList: [],
    goodsList: []
  },
  // 数据懒加载的当前请求次数
  current: 1,
  // 保存商品列表数据的总条数
  total: null,
  //生命周期函数--监听页面初次渲染完成
  onReady: function () {
    this.renderBanner()
    this.renderGoods()
  },
  //页面上拉触底事件的处理函数
  onReachBottom: function () {
    if (this.data.goodsList.length!=this.total) {
      // 证明数据还没请求完,下拉还可以获取数据
      this.current++
      this.renderGoods()
    }
  },
  // 获取轮播图数据
  renderBanner() {
    request({
      url: '/recommends'
    }).then(res => {
      this.setData({
        bannerList: res
      })
    })
  },
  // 获取首页商品列表数据
  renderGoods() {
    request({
      url: `/goods?_page=${this.current}&_limit=5`
    },true).then(res => {
      this.total = Number(res.total)
      this.setData({
        goodsList: [...this.data.goodsList,...res.list],
      })
    })
  },
  // 跳转到商品详情页面
  goodsClick(e) {
    let id = e.currentTarget.dataset.id
    let name = e.currentTarget.dataset.name
    wx.navigateTo({
      url: `/pages/goodsdetail/goodsdetail?id=${id}&name=${name}`
    })
  },
  // 跳转到搜索页面
  searchClick() {
    wx.navigateTo({
      url: '/pages/search/search'
    })
  }
})