import request from "../../util/request"

// pages/bindtel/bindtel.js
Page({
  data: {
    tel: ''
  },
  formInputChange(e) {
    this.setData({
      tel: e.detail.value
    })
  },
  submitForm() {
    let tel = this.data.tel
    let storage = wx.getStorageSync('token')
    wx.setStorageSync('tel', tel)
    request({
      url: `/users?tel=${tel}&nickName=${storage.nickName}`
    }).then(res => {
      if (res.length===0) {
        // 证明是新用户，此时向服务器发起请求，添加上这个新用户
        request({
          url: '/users',
          method: 'POST',
          data: {
            ...storage,
            tel: tel
          }
        }).then(res => {
          wx.navigateBack({
            delta: 2,
          })
        })
      } else {
        // 这里代表是老用户，有可能是退出了账号或者用新的手机设备重新登录，但此时服务器已经有该账号的信息，所以直接回退到刚才的界面
        wx.navigateBack({
          delta: 2,
        })
      }
    })
  }
})