import request from "../../util/request"

// pages/searchlist/searchlist.js
Page({
  data: {
    goodsList: [],
    tagList: ['价格排序','好评排序'],
    tagcurrent: -1
  },
  priceOrder: true,
  commentOrder: true,
  onLoad: function (options) {
    this.getList(options.id)
  },
  getList(id) {
    request({
      url: `/categories/${id}?_embed=goods`
    }).then(res => {
      this.setData({
        goodsList: res.goods
      })
    })
  },
  goodsClick(e) {
    let id = e.currentTarget.dataset.id
    let name = e.currentTarget.dataset.name
    wx.navigateTo({
      url: `/pages/goodsdetail/goodsdetail?id=${id}&name=${name}`,
    })
  },
  // 价格排序
  handlePrice() {
    this.setData({
      goodsList: this.priceOrder?this.data.goodsList.sort((item1,item2) => item2.price-item1.price):this.data.goodsList.sort((item1,item2) => item1.price-item2.price),
      tagcurrent: 0
    })
    this.priceOrder = !this.priceOrder
  },
  // 好评排序
  handleComment() {
    this.setData({
      goodsList: this.commentOrder?this.data.goodsList.sort((item1,item2) => parseInt(item2.goodcomment)-parseInt(item1.goodcomment)):this.data.goodsList.sort((item1,item2) => parseInt(item1.goodcomment)-parseInt(item2.goodcomment)),
      tagcurrent: 1
    })
    this.commentOrder = !this.commentOrder
  }
})