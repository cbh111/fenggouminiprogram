import request from "../../util/request"
Page({
  data: {

  },
  onLoad: function (options) {
    this.setData({
      search: this.search.bind(this)
    })
  },
  search(value) {
    return Promise.all([
      request({
        url: `/categories?title_like=${value}`
      }),
      request({
        url: `/goods?title_like=${value}`
      })
    ]).then(res => {
      // 箭头函数中，若返回的值是一个对象，则要在箭头后加一个小括号，否则对象的花括号就会被当成返回函数的那个花括号了。不加小括号只能写成item=>{returun {} }
      return [...res[0].map(item=>({...item,text: item.title,type:1})),...res[1].map(item=>({...item,text: item.title,type:2}))]
    })
  },
  selectResult(e) {
    if (e.detail.item.type===1) {
      //搜索列表
      wx.navigateTo({
        url: `/pages/searchlist/searchlist?id=${e.detail.item.id}`
      })
    } else {
      // 商品详情列表
      wx.navigateTo({
        url: `/pages/goodsdetail/goodsdetail?id=${e.detail.item.id}&name=${e.detail.item.title}`,
      })
    }
  }
})