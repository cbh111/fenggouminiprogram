// pages/shopcar/shopcar.js
import checkAuth from '../../util/auth'
import request from '../../util/request'
Page({
  data: {
    isleft: -1,
    cartsList: []
  },
  xstar: 0,
  xmove: 0,
  // 生命周期函数--监听页面隐藏
  onHide: function () {
    // 这里是如果用户向左滑动了以后，没有滑动回去，删除按钮显示的时候，切换到别的页面，那么就让切换到别的页面以前把删除按钮给它隐藏回去在切出去别的页面，这样用户回来的时候看不到删除按钮
    this.setData({
      isleft: -1
    })
  },
  // 生命周期函数--监听页面显示
  onShow: function () {
    checkAuth(() => {
      let {nickName} = wx.getStorageSync('token')
      let tel = wx.getStorageSync('tel')
      request({
        url: `/carts?_expand=good&username=${nickName}&tel=${tel}`
      }).then(res => {
        this.setData({
          cartsList: res
        })
      })
    })
  },
  // hStart和hMove方法是判断用户左右滑动显示和隐藏删除按钮
  hStart(e) {
    this.xstar = e.touches[0].pageX
  },
  hMove(e) {
    this.xmove = e.touches[0].pageX
    let x = this.xstar - this.xmove
    if (x>20) {
      this.setData({
        isleft: e.currentTarget.dataset.id
      })
    } else {
      this.setData({
        isleft: -1
      })
    }
  },
  // 删除商品
  delgoods(e) {
    let id = e.currentTarget.dataset.id
    this.setData({
      cartsList: this.data.cartsList.filter(item => item.id!==id)
    })
    request({
      url: `/carts/${id}`,
      method: 'DELETE'
    }).then(res => {
      this.setData({
        isleft: -1
      })
      wx.showToast({
        title: '删除成功',
      })
    })
  },
  // 复选框的勾选事件
  checkClick(e) {
    let item = e.currentTarget.dataset.item
    item.checked = !item.checked
    // 调用统一的方法，改变data中的cartsList数组，来更新视图
    this.handleUpdata(item)
  },
  // 封装的统一更新视图方法
  handleUpdata(item) {
    // 这里只做了本地的视图更新，服务器里并没有更新，所以下边还要发送ajax请求，服务器里也要同步更新
    this.setData({
      cartsList: this.data.cartsList.map(data => {
        if (data.id == item.id) {
          return item
        }
        return data
      })
    })
    // 发情更新请求
    request({
      url: `/carts/${item.id}`,
      method: 'PUT',
      // 注意这里不能使用...item，因为展开item的话，现在item是通过`/carts?_expand=good&username:${nickName}&tel=${tel}`这个接口拿到的，所以里面还多了一个good属性，而我们现在更新的接口是carts，carts里没有good属性，我们也不要加，因为good是个数组里边有很多数据，而且加进去了也没用。所以这里要一个一个的把属性写上(因为put方法会覆盖之前的数据，我们这里主要更新一个属性的值，但是如果不写上之前的属性，那么item里就会只有一个属性了，这里json-server不支持catch请求，如果支持catch请求就不会覆盖之前的值了，只写一个属性就可以)
      data: {
        username: item.username,
        tel: item.tel,
        goodId: item.goodId,
        number: item.number,
        checked: item.checked
      }
    })
  },
  // 商品数量添加
  handleAdd(e) {
    let item = e.currentTarget.dataset.item
    if (item.number == 10) {
      wx.showToast({
        title: '不能在加了哟',
        icon: 'none'
      })
      return
    }
    item.number++
    this.handleUpdata(item)
  },
  // 商品数量减少
  handleReduce(e) {
    let item = e.currentTarget.dataset.item
    if (item.number == 1) {
      wx.showToast({
        title: '不能在减了哟',
        icon: 'none'
      })
      return
    }
    item.number--
    this.handleUpdata(item)
  },
  // 全选按钮
  handleAllChecked(e) {
    if (e.detail.value.length == 1) {
      // 代表全选了
      this.setData({
        cartsList: this.data.cartsList.map(item => ({
          ...item,
          checked: true
        }))
      })
      // 这里应该向后端发起修改的ajax请求，但应为json-server没有这个接口功能，所以暂时先不做
    } else {
      // 代表没有全选
      this.setData({
        cartsList: this.data.cartsList.map(item => ({
          ...item,
          checked: false
        }))
      })
      // 这里也应该向后端发起修改的ajax请求，但应为json-server没有这个接口功能，所以暂时先不做
    }
  },
  payClick() {
    wx.showToast({
      title: '暂未开放付款',
      icon: 'none'
    })
  }
})