// pages/center/center.js
import checkAuth from '../../util/auth'
Page({
  data: {
    userInfo: null
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    checkAuth(() => {
      this.setData({
        userInfo: wx.getStorageSync('token')
      })
    })
  },
  replaceImage() {
    wx.chooseMessageFile({
      count: 10,
      type: 'image',
      success: (res) => {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFiles
        this.setData({
          userInfo: {
            ...this.data.userInfo,
            avatarUrl: tempFilePaths[0].path
          }
        })
      }
    })
  },
  outLogin() {
    wx.removeStorage({
      key: 'tel'
    })
    wx.removeStorage({
      key: 'token'
    }).then(res => {
      wx.showToast({
        title: '退出成功',
        icon: 'none'
      })
      setTimeout(() => {
        wx.switchTab({
          url: '/pages/home/home'
        })
      },1000)
    })
  }
})