// pages/wxshouquan/wxshouquan.js
Page({
  data: {

  },
  handelAuth() {
    wx.getUserProfile({
      desc: '用户完善会员资料',
      success(res) {
        wx.setStorageSync('token', res.userInfo)
        wx.navigateTo({
          url: '/pages/bindtel/bindtel'
        })
      }
    })
  }
})