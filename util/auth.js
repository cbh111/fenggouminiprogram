// 1.判断本地存储是否有手机号信息，如果有直接加入
// 2.没有手机号，判断是否有token信息（有token证明微信已授权），如果有，引导跳到手机号绑定页面
// 3.没有token授权信息，我们引导用户授权页面
// 由于调用手机号授权需要微信的企业认证，个人用户无法使用api调用，所以在本项目中写一个绑定手机号页面来代替
function checkAuth(callback) {
  if (wx.getStorageSync('tel')) {
    // 这里代表已授权并且绑定手机号，处理正常业务
    callback()
  } else {
    if (wx.getStorageSync('token')) {
      // 这里代表微信已经授权，但未绑定手机号
      wx.navigateTo({
        url: '/pages/bindtel/bindtel'
      })
    } else {
      // 这里代表微信还未授权
      wx.navigateTo({
        url: '/pages/wxshouquan/wxshouquan'
      })
    }
  }
}

export default checkAuth