// 第二个参数是是否返回响应头中的信息,例如数据懒加载的时候会用到响应头中的数据总条数
function request(params,isHeader=false) {
  // 显示loading
  wx.showLoading({
    title: '加载中',
  })
  return new Promise((resolve,reject) => {
    wx.request({
      ...params,
      url: 'http://localhost:5000' + params.url,
      success: (res) => {
        if (isHeader) {
          resolve({
            list: res.data,
            total: res.header['X-Total-Count']
          })
          return
        }
        resolve(res.data)
      },
      fail: (err) => {
        reject(err)
      },
      complete() {
        // 不管请求是否成功,promise都会执行这里
        // 所以在这里把loading的toast关闭
        wx.hideLoading()
      }
    })
  })
}

export default request