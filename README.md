# 峰购小程序

#### 介绍

一款简约而不简单的峰购微信小程序，如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！

后端代码在server文件夹下

#### 后端启动方法

```
1、npm install -g json-server
2、在server文件夹下执行：json-server --watch db.json --port 5000
```

#### 部分截图展示
![截图展示](https://images.gitee.com/uploads/images/2021/1029/203412_de01e7a6_5695113.png "1.png")

![截图展示](https://images.gitee.com/uploads/images/2021/1029/203432_10c79faf_5695113.png "2.png")

![截图展示](https://images.gitee.com/uploads/images/2021/1029/203443_05897578_5695113.png "3.png")

![截图展示](https://images.gitee.com/uploads/images/2021/1029/203451_f85abc0f_5695113.png "4.png")

![截图展示](https://images.gitee.com/uploads/images/2021/1029/203502_66c90e24_5695113.png "5.png")

![截图展示](https://images.gitee.com/uploads/images/2021/1029/203514_6dea213b_5695113.png "6.png")

####  结尾

感谢您的支持，如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！ 技术交流QQ：2678467517